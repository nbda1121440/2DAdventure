using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Event/SelectButtonEventSO")]
public class SelectButtonEventSO : ScriptableObject
{
    public UnityAction<Button> selectButtonEvent;

    /// <summary>
    /// 选择按钮请求
    /// </summary>
    /// <param name="locationToLoad">要加载的按钮</param>
    public void RaiseSelectButtonEvent(Button button)
    {
        selectButtonEvent?.Invoke(button);
    }
}
