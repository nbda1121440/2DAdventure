using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class SavePointRock : MonoBehaviour, IInteractable
{
    [Header("广播")]
    public VoidEventSO saveDataEvent;

    [Header("变量参数")]
    public SpriteRenderer childSpriteRenderer;
    public Light2D light2D;
    public Sprite darkSprite;
    public Sprite lightSprite;
    public bool isDone = false;

    private void OnEnable()
    {
        childSpriteRenderer.sprite = isDone ? lightSprite : darkSprite;
        light2D.gameObject.SetActive(isDone);
    }

    public void TriggerAction()
    {
        LightUpRock();
    }

    /// <summary>
    /// 点亮石头
    /// </summary>
    private void LightUpRock()
    {
        if (isDone)
        {
            return;
        }

        childSpriteRenderer.sprite = lightSprite;
        isDone = true;
        light2D.gameObject.SetActive(true);
        gameObject.tag = "Untagged";

        // 保存数据
        saveDataEvent.RaiseEvent();
    }
}
