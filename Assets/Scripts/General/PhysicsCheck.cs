using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PhysicsCheck : MonoBehaviour
{
    private CapsuleCollider2D coll;
    private PlayerController playerController;
    private Rigidbody2D rb;

    [Header("监测参数")]
    public bool manual;
    public bool isPlayer;
    public Vector2 bottomOffset;
    public Vector2 slideBottomOffset;
    public Vector2 bottomLeftOffset;
    public Vector2 bottomRightOffset;
    public Vector2 leftOffset;
    public Vector2 rightOffset;
    public float checkRaduis;
    public LayerMask groundLayer;

    [Header("状态")]
    public bool isGround;
    public bool isLeftGround;
    public bool isRightGround;
    public bool touchLeftWall;
    public bool touchRightWall;
    public bool onWall;

    private void Awake()
    {
        coll = GetComponent<CapsuleCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        
        if (isPlayer)
        {
            playerController = GetComponent<PlayerController>();
        }
    }

    void Update()
    {
        Check();
    }

    public void Check()
    {
        if (!manual)
        {
            int faceDir = 0;
            if (transform.localScale.x > 0)
            {
                faceDir = 1;
            }
            else if (transform.localScale.x < 0)
            {
                faceDir = -1;
            }

            bottomOffset = new Vector2(faceDir * coll.offset.x, coll.bounds.size.y / 2 - coll.offset.y);
            slideBottomOffset = new Vector2(faceDir * coll.offset.x, coll.bounds.size.y / 2 - coll.offset.y + checkRaduis);
            bottomLeftOffset = new Vector2(faceDir * coll.offset.x - coll.bounds.size.x / 2, coll.bounds.size.y / 2 - coll.offset.y);
            bottomRightOffset = new Vector2(faceDir * coll.offset.x + coll.bounds.size.x / 2, coll.bounds.size.y / 2 - coll.offset.y);
            leftOffset = new Vector2(faceDir * coll.offset.x - coll.bounds.size.x / 2, coll.bounds.size.y / 2);
            rightOffset = new Vector2(faceDir * coll.offset.x + coll.bounds.size.x / 2, coll.bounds.size.y / 2);
        }

        // 监测地面
        if (onWall)
        {
            isGround = Physics2D.OverlapCircle((Vector2)transform.position + slideBottomOffset, checkRaduis, groundLayer);
        }
        else
        {
            isGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, checkRaduis, groundLayer);
        }
        isLeftGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomLeftOffset, checkRaduis, groundLayer);
        isRightGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomRightOffset, checkRaduis, groundLayer);

        // 墙体判断
        touchLeftWall = Physics2D.OverlapCircle((Vector2)transform.position + leftOffset, checkRaduis, groundLayer);
        touchRightWall = Physics2D.OverlapCircle((Vector2)transform.position + rightOffset, checkRaduis, groundLayer);

        // 在墙壁上
        if (isPlayer)
        {
            // 贴左墙有个向左的力，或者贴右墙有个向右的力
            // 并且有个向下的速度
            // 此时才算在墙上
            onWall = ((touchLeftWall && playerController.inputDirection.x < 0f) || 
                (touchRightWall && playerController.inputDirection.x > 0f)) && 
                rb.velocity.y < 0;
        }
    }

    private void OnDrawGizmosSelected()
    {
        // 绘制监测区
        Gizmos.DrawWireSphere((Vector2)transform.position + bottomOffset, checkRaduis);
        Gizmos.DrawWireSphere((Vector2)transform.position + bottomLeftOffset, checkRaduis);
        Gizmos.DrawWireSphere((Vector2)transform.position + bottomRightOffset, checkRaduis);
        Gizmos.DrawWireSphere((Vector2)transform.position + leftOffset, checkRaduis);
        Gizmos.DrawWireSphere((Vector2)transform.position + rightOffset, checkRaduis);
        // 绘制滑墙的时候的脚底监测点
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere((Vector2)transform.position + slideBottomOffset, checkRaduis);
    }
}
