using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuCanvas : MonoBehaviour
{
    public Button selectButton;
    public SelectButtonEventSO selectButtonEvent;

    //void Start()
    //{
    //    selectButtonEvent.RaiseSelectButtonEvent(selectButton);
    //}

    private void OnEnable()
    {
        EventSystem.current.SetSelectedGameObject(selectButton.gameObject);
    }

    public void ExitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
