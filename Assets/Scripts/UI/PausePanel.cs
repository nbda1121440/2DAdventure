using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PausePanel : MonoBehaviour
{
    public VoidEventSO pauseEvent;
    public Slider slider;
    public AudioManager audioManager;

    private void OnEnable()
    {
        pauseEvent.OnEventRaised += OnPause;
    }

    private void OnDisable()
    {
        pauseEvent.OnEventRaised -= OnPause;
    }

    private void OnPause()
    {
        float volume = audioManager.GetVolume();
        slider.value = (volume + 80) / 100;
    }
}
