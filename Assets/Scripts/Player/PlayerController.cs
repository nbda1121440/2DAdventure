using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Scripting.APIUpdating;

public class PlayerController : MonoBehaviour
{
    [Header("监听事件")]
    public SceneLoadEventSO sceneLoadEvent;
    public VoidEventSO afterSceneLoadedEvent;
    public VoidEventSO loadDataEvent;
    public VoidEventSO backToMenuEvent;

    public PlayerInputControl inputControl;
    private Rigidbody2D rb;
    private CapsuleCollider2D coll;
    private PhysicsCheck physicsCheck;
    private PlayerAnimation playerAnimation;
    private Character character;
    public Vector2 inputDirection;

    [Header("基本参数")]
    public float speed;
    private float runSpeed;
    private float walkSpeed;
    public float jumpForce;
    public float wallJumpForce;
    public float wallSlideSpeed;

    private Vector2 originalOffset;
    private Vector2 originalSize;

    public float hurtForce;
    public float slideDistance;
    public float slideSpeed;
    public int slidePowerCost;

    [Header("物理材质")]
    public PhysicsMaterial2D normal;
    public PhysicsMaterial2D wall;

    [Header("状态")]
    public bool isCrouch;
    public bool isHurt;
    public bool isDead;
    public bool isAttack;
    public bool wallJump;
    public bool isSlide;

    private void Awake()
    {
        // 获取 Player 上面的刚体组件
        rb = GetComponent<Rigidbody2D>();
        physicsCheck = GetComponent<PhysicsCheck>();

        // 获取碰撞体，以及碰撞体相关的参数
        coll = GetComponent<CapsuleCollider2D>();
        originalOffset = coll.offset;
        originalSize = coll.size;

        playerAnimation = GetComponent<PlayerAnimation>();
        character = GetComponent<Character>();

        inputControl = new PlayerInputControl();

        // 跳跃：按下键盘上的空格键，或者手柄上的 EAST 键，就会触发 Jump 函数
        inputControl.Gameplay.Jump.started += Jump;

        #region 强制走路
        runSpeed = speed;
        walkSpeed = speed / 5;
        // 一直键盘上的左shift键，或者手柄上的左肩键，就会让人物以 walkSpeed 走路，否则就会让人物以 runSpeed 跑步
        inputControl.Gameplay.WalkButton.performed += ctx =>
        {
            if (physicsCheck.isGround)
            {
                speed = walkSpeed;
            }
        };
        inputControl.Gameplay.WalkButton.canceled += ctx =>
        {
            if (physicsCheck.isGround)
            {
                speed = runSpeed;
            }
        };
        #endregion

        // 攻击：按下键盘上的 J 键，或者手柄上的 A 键，就会触发 PlayerAttack 函数
        inputControl.Gameplay.Attack.started += PlayerAttack;

        // 滑铲：按下键盘上的 K 键，或者手柄上的 LB 键，就会触发 Slide 函数
        inputControl.Gameplay.Slide.started += Slide;
    }

    private void OnEnable()
    {
        inputControl.Enable();
        sceneLoadEvent.loadRequestEvent += OnSceneLoadEvent;
        // Debug.Log("开始监听 OnAfterSceneLoadedEvent");
        afterSceneLoadedEvent.OnEventRaised += OnAfterSceneLoadedEvent;
        loadDataEvent.OnEventRaised += OnLoadDataEvent;
        backToMenuEvent.OnEventRaised += OnLoadDataEvent;
    }

    private void OnDisable()
    {
        inputControl.Disable();
        sceneLoadEvent.loadRequestEvent -= OnSceneLoadEvent;
        // Debug.Log("取消监听 OnAfterSceneLoadedEvent");
        afterSceneLoadedEvent.OnEventRaised -= OnAfterSceneLoadedEvent;
        loadDataEvent.OnEventRaised -= OnLoadDataEvent;
        backToMenuEvent.OnEventRaised -= OnLoadDataEvent;
    }

    private void Update()
    {
        inputDirection = inputControl.Gameplay.Move.ReadValue<Vector2>();

        CheckState();
    }

    private void FixedUpdate()
    {
        // 在 FixedUpdate 里面对刚体进行操作
        if (!isHurt && !isAttack)
        {
            Move();
        }
    }

    //private void OnTriggerStay2D(Collider2D other)
    //{
    //    Debug.Log(other.name);
    //}

    // 场景加载过程停止控制
    private void OnSceneLoadEvent(GameSceneSO arg0, Vector3 arg1, bool arg2)
    {
        // Debug.Log("禁用输入");
        inputControl.Gameplay.Disable();
    }

    // 场景加载结束恢复控制
    private void OnAfterSceneLoadedEvent()
    {
        // Debug.Log("启动输入");
        inputControl.Gameplay.Enable();
    }

    // 加载数据的时候，需要设置 dead 动画
    private void OnLoadDataEvent()
    {
        isDead = false;
    }

    public void Move()
    {
        // 不在地面的时候也要能移动，否则滑墙实现不了
        //if (physicsCheck.isGround)
        //{
        //    // 人物在地面上的时候才能够改变速度以及转向

            if (!isCrouch && !wallJump)
            {
                // 人物没有下蹲的时候，才能进行移动

                // 设置刚体的速度，这样刚体就能移动了
                rb.velocity = new Vector2(inputDirection.x * speed, rb.velocity.y);
            }

            // 初始人物是面朝 x 轴正方向的
            int faceDir = 0;
            if (transform.localScale.x > 0)
            {
                faceDir = 1;
            }
            else if (transform.localScale.x < 0)
            {
                faceDir = -1;
            }
            // 输入是 x 轴正方向的时候面朝也是 x 轴正方向
            // 输入是 x 轴负方向的时候面朝也是 x 轴负方向
            if (inputDirection.x > 0)
            {
                faceDir = 1;
            }
            if (inputDirection.x < 0)
            {
                faceDir = -1;
            }

            // 人物翻转
            transform.localScale = new Vector3(faceDir * Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);

            // 下蹲
            isCrouch = inputDirection.y < -0.5f;
            if (isCrouch)
            {
                // 修改碰撞体大小和位移
                coll.size = new Vector2(originalSize.x, originalSize.y - 0.3f);
                coll.offset = new Vector2(originalOffset.x, originalOffset.y - 0.15f);
            }
            else
            {
                // 还原之前碰撞体参数
                coll.size = originalSize;
                coll.offset = originalOffset;
            }
        //}
    }

    private void Jump(InputAction.CallbackContext context)
    {
        // Debug.Log("JUMP");
        GetComponent<AudioDefination>()?.PlayAudioClip();
        if (physicsCheck.isGround)
        {
            // 只有在地面上才能进行跳跃

            // 这里使用的是 ForceMode2D.Impulse，表示瞬间修改人物上方向的速度为 jumpForce 对应的数值
            rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);

            // 动画状态机是没有办法停止滑铲协程的，需要强制停止滑铲协程
            isSlide = false;
            StopAllCoroutines();
        }
        else if (physicsCheck.onWall)
        {
            wallJump = true;
            Vector2 forceDir = new Vector2(-inputDirection.x, 1f) * wallJumpForce;
            Debug.Log($"wallJump = true, forceDir = {forceDir}");
            rb.AddForce(forceDir, ForceMode2D.Impulse);
        }
    }

    private void PlayerAttack(InputAction.CallbackContext context)
    {
        if (physicsCheck.isGround)
        {
            // 只有在地面上才能进行攻击
            playerAnimation.PlayAttack();
            isAttack = true;

            //// 攻击的时候禁止移动
            //rb.velocity = Vector2.zero;
        }
    }

    private void Slide(InputAction.CallbackContext context)
    {
        if (!isSlide && physicsCheck.isGround && character.currentPower >= slidePowerCost)
        {
            isSlide = true;

            var targetPos = new Vector3(transform.position.x + slideDistance * transform.localScale.x, transform.position.y);

            gameObject.layer = LayerMask.NameToLayer("Enemy");
            StartCoroutine(TriggerSlide(targetPos));

            character.OnSlide(slidePowerCost);
        }
    }

    private IEnumerator TriggerSlide(Vector3 target)
    {
        do
        {
            yield return null;
            if (!physicsCheck.isGround)
            {
                // 滑铲的时候不是在地面上，就退出协程
                break;
            }
            if ((physicsCheck.touchLeftWall && transform.localScale.x < 0f) || 
                (physicsCheck.touchRightWall && transform.localScale.x > 0f))
            {
                // 面朝左撞左墙 或者 面朝右撞右墙，退出循环
                isSlide = false;
                break;
            }

            // 根据滑动的速度，让刚体移动到指定位置
            rb.MovePosition(new Vector2(transform.position.x + transform.localScale.x * slideSpeed, transform.position.y));
        } while (Mathf.Abs(target.x - transform.position.x) > 0.1f);
        isSlide = false;
        gameObject.layer = LayerMask.NameToLayer("Player");
    }

    #region UnityEvent
    public void GetHurt(Transform attacker)
    {
        isHurt = true;
        rb.velocity = Vector2.zero;
        Vector2 dir = new Vector2(transform.position.x - attacker.position.x, 0).normalized;
        rb.AddForce(dir * hurtForce, ForceMode2D.Impulse);
    }

    public void PlayerDead()
    {
        isDead = true;
        inputControl.Gameplay.Disable();
    }
    #endregion

    private void CheckState()
    {
        coll.sharedMaterial = physicsCheck.isGround ? normal : wall;

        if (physicsCheck.onWall)
        {
            if (physicsCheck.isGround)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
            }
            else if (wallJump)
            {
                // 速度不要变
            }
            else
            {
                rb.velocity = new Vector2(rb.velocity.x, -1 * wallSlideSpeed);
            }
        }
        else
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y);
        }

        if (wallJump && rb.velocity.y < 0)
        {
            Debug.Log("wallJump = false");
            wallJump = false;
        }
    }
}
