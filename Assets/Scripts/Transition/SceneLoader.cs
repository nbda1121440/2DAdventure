using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour, ISaveable
{
    public Transform playerTrans;
    public Vector3 firstPosition;
    public Vector3 menuPosition;

    [Header("事件监听")]
    public SceneLoadEventSO loadEventSO;
    public VoidEventSO newGameEventSO;
    public SceneLoadEventSO unloadEventSO;
    public VoidEventSO backToMenuEventSO;

    [Header("广播")]
    public VoidEventSO afterSceneLoadedEvent;
    public FadeEventSO fadeEvent;

    [Header("场景")]
    public GameSceneSO firstLoadScene;
    public GameSceneSO menuScene;
    private GameSceneSO currentLoadScene;
    private GameSceneSO locationToLoad;
    private Vector3 posToGo;
    private bool fadeScreen;
    private bool isLoading;
    public float fadeDuration;

    private void Awake()
    {
        // Addressables.LoadSceneAsync(firstLoadScene.sceneReference, LoadSceneMode.Additive);
        //currentLoadScene = firstLoadScene;
        //currentLoadScene.sceneReference.LoadSceneAsync(LoadSceneMode.Additive);
    }

    private void Start()
    {
        // NewGame();
        loadEventSO.RaiseLoadRequestEvent(menuScene, menuPosition, true);
    }

    private void OnEnable()
    {
        loadEventSO.loadRequestEvent += OnLoadRequestEvent;
        newGameEventSO.OnEventRaised += NewGame;
        backToMenuEventSO.OnEventRaised += BackToMenuEvent;

        ISaveable saveable = this;
        saveable.RegisterSaveData();
    }

    private void OnDisable()
    {
        loadEventSO.loadRequestEvent -= OnLoadRequestEvent;
        newGameEventSO.OnEventRaised -= NewGame;
        backToMenuEventSO.OnEventRaised -= BackToMenuEvent;

        ISaveable saveable = this;
        saveable.UnRegisterSaveData();
    }

    private void NewGame()
    {
        locationToLoad = firstLoadScene;
        // OnLoadRequestEvent(locationToLoad, firstPosition, true);
        loadEventSO.RaiseLoadRequestEvent(locationToLoad, firstPosition, true);
    }

    private void BackToMenuEvent()
    {
        loadEventSO.RaiseLoadRequestEvent(menuScene, menuPosition, true);
    }

    /// <summary>
    /// 场景加载事件请求
    /// </summary>
    /// <param name="locationToLoad"></param>
    /// <param name="posToGo"></param>
    /// <param name="fadeScreen"></param>
    private void OnLoadRequestEvent(GameSceneSO locationToLoad, Vector3 posToGo, bool fadeScreen)
    {
        if (isLoading)
        {
            return;
        }
        isLoading = true;
        this.locationToLoad = locationToLoad;
        this.posToGo = posToGo;
        this.fadeScreen = fadeScreen;

        if (currentLoadScene != null)
        {
            StartCoroutine(UnLoadPreviousScene());
        }
        else
        {
            LoadNewScene();
        }
    }

    private IEnumerator UnLoadPreviousScene()
    {
        if (fadeScreen)
        {
            // 蒙版变黑
            fadeEvent.FadeIn(fadeDuration);
        }

        yield return new WaitForSeconds(fadeDuration);

        // 表示场景已经完全卸载了，开始加载新场景
        unloadEventSO.RaiseLoadRequestEvent(locationToLoad, posToGo, true);

        if (currentLoadScene != null)
        {
            yield return currentLoadScene.sceneReference.UnLoadScene();
        }

        // 关闭人物
        playerTrans.gameObject.SetActive(false);

        // 加载新场景
        LoadNewScene();
    }

    private void LoadNewScene()
    {
        var loadingOption = locationToLoad.sceneReference.LoadSceneAsync(LoadSceneMode.Additive, true);
        loadingOption.Completed += OnLoadCompleted;
    }

    /// <summary>
    /// 场景加载完成后
    /// </summary>
    /// <param name="handle"></param>
    /// <exception cref="NotImplementedException"></exception>
    private void OnLoadCompleted(AsyncOperationHandle<SceneInstance> obj)
    {
        currentLoadScene = locationToLoad;

        playerTrans.position = posToGo;

        playerTrans.gameObject.SetActive(true);
        if (currentLoadScene.sceneType == SceneType.Menu)
        {
            // 菜单模式下禁用人物输入
            playerTrans.GetComponent<PlayerController>()?.inputControl.Gameplay.Disable();
        }

        if (fadeScreen)
        {
            // 蒙版变透明
            fadeEvent.FadeOut(fadeDuration);
        }

        isLoading = false;

        if (currentLoadScene.sceneType == SceneType.Location)
        {
            // 场景完成后的事件
            // Debug.Log("触发 afterSceneLoadedEvent");
            afterSceneLoadedEvent.RaiseEvent();
        }
    }

    public DataDefination GetDataID()
    {
        return this.GetComponent<DataDefination>();
    }

    public void SaveData(Data data)
    {
        data.SaveGameScene(currentLoadScene);
    }

    public void LoadData(Data data)
    {
        // 读取的时候首先要确保存在数据
        // 如果玩家的坐标保存过了，那么就应该能获取到存储的场景信息了
        var playerID = playerTrans.GetComponent<DataDefination>().ID;
        if (data.characterPosDict.ContainsKey(playerID))
        {
            locationToLoad = data.GetSavedScene();
            posToGo = data.characterPosDict[playerID].ToVector3();
            OnLoadRequestEvent(locationToLoad, posToGo, true);
        }
    }
}
