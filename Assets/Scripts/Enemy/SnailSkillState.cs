using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailSkillState : BaseState
{
    public override void OnEnter(Enemy enemy)
    {
        currentEnemy = enemy;
        currentEnemy.currentSpeed = 0;
        currentEnemy.anim.SetBool("hide", true);
        currentEnemy.anim.SetTrigger("skill");

        // 进入缩壳状态的时候变无敌
        currentEnemy.GetComponent<Character>().invulnerable = true;
        currentEnemy.GetComponent<Character>().invulnerableCounter = currentEnemy.GetComponent<Character>().invulnerableDuration;
    }

    public override void LogicUpdate()
    {
        if (currentEnemy.lostTimeCounter <= 0)
        {
            currentEnemy.SwitchState(NPCState.Patrol);
        }

        // 为了防止退出 invulnerble 状态，需要重置 invulnerableCounter
        currentEnemy.GetComponent<Character>().invulnerableCounter = currentEnemy.GetComponent<Character>().invulnerableDuration;
    }

    public override void PhysicsUpdate()
    {
        
    }

    public override void OnExit()
    {
        // 退出缩壳状态的时候取消无敌
        currentEnemy.GetComponent<Character>().invulnerable = false;

        currentEnemy.anim.SetBool("hide", false);
    }
}
