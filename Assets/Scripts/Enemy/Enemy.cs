using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator), typeof(PhysicsCheck))]
public class Enemy : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody2D rb;
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public PhysicsCheck physicsCheck;

    [Header("基本参数")]
    public float normalSpeed;
    public float chaseSpeed;
    [HideInInspector]
    public float currentSpeed;
    public Vector3 faceDir;
    public float hurtForce;
    public Transform attacker;
    public Vector3 spwanPoint;

    [Header("检测")]
    public Vector2 centerOffset;
    public Vector2 checkSize;
    public float checkDistance;
    public LayerMask attackLayer;

    [Header("计时器")]
    public float waitTime;
    public float waitTimeCounter;
    public bool wait;
    public float lostTime;
    public float lostTimeCounter;

    [Header("状态")]
    public bool isHurt;
    public bool isDead;

    private BaseState currentState;
    protected BaseState patrolState;
    protected BaseState chaseState;
    protected BaseState skillState;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        physicsCheck = GetComponent<PhysicsCheck>();
        currentSpeed = normalSpeed;
        waitTimeCounter = waitTime;
        spwanPoint = transform.position;
    }

    private void OnEnable()
    {
        currentState = patrolState;
        currentState.OnEnter(this);
    }

    private void Update()
    {
        if (transform.localScale.x > 0)
        {
            // 此时野猪是朝左的
            faceDir = new Vector3(-1, 0, 0);
        }
        else if (transform.localScale.x < 0)
        {
            // 此时野猪是朝右的
            faceDir = new Vector3(1, 0, 0);
        }

        currentState.LogicUpdate();
        TimeCounter();
    }

    private void FixedUpdate()
    {
        if (!isHurt && 
            !isDead && 
            !wait &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("snailPreMove") &&
            !anim.GetCurrentAnimatorStateInfo(0).IsName("snailRecover"))
        {
            Move();
        }

        currentState.PhysicsUpdate();
    }

    private void OnDisable()
    {
        currentState.OnExit();
    }

    public virtual void Move()
    {
        rb.velocity = new Vector2(currentSpeed * faceDir.x, rb.velocity.y);
    }

    /// <summary>
    /// 计时器
    /// </summary>
    public void TimeCounter()
    {
        if (wait)
        {
            waitTimeCounter -= Time.deltaTime;
            if (waitTimeCounter <= 0)
            {
                wait = false;
                waitTimeCounter = waitTime;
                transform.localScale = new Vector3(faceDir.x * Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            }
        }

        if (!FoundPlayer() && lostTimeCounter > 0)
        {
            lostTimeCounter -= Time.deltaTime;
        }
        else if (FoundPlayer())
        {
            // 添加这个额外的判断，在发现玩家的时候重置计数器
            lostTimeCounter = lostTime;
        }
    }

    public virtual bool FoundPlayer()
    {
        return Physics2D.BoxCast(transform.position + (Vector3)centerOffset, checkSize, 0, faceDir, checkDistance, attackLayer);
    }

    public void SwitchState(NPCState state)
    {
        var newState = state switch
        {
            NPCState.Patrol => patrolState,
            NPCState.Chase => chaseState,
            NPCState.Skill => skillState,
            _ => null
        };

        currentState.OnExit();
        currentState = newState;
        newState.OnEnter(this);
    }

    public virtual Vector3 GetNewPoint()
    {
        return transform.position;
    }

    #region 事件执行方法

    public void OnTakeDamage(Transform attackTrans)
    {
        attacker = attackTrans;

        // 转身
        if (attackTrans.position.x - transform.position.x > 0)
        {
            // 攻击者在敌人右侧，此时 scale.x = -1
            transform.localScale = new Vector3(-1 * Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
        if (attackTrans.position.x - transform.position.x < 0)
        {
            // 攻击者在敌人左侧，此时 scale.x = 1
            transform.localScale = new Vector3(1 * Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }

        // 受伤被击退
        isHurt = true;
        anim.SetTrigger("hurt");
        rb.velocity = new Vector2(0, rb.velocity.y);
        Vector2 dir = new Vector2(transform.position.x - attackTrans.position.x, 0).normalized;
        StartCoroutine(OnHurt(dir));
    }

    IEnumerator OnHurt(Vector2 dir)
    {
        rb.AddForce(dir * hurtForce, ForceMode2D.Impulse);
        yield return new WaitForSeconds(0.5f);
        // 个人感觉用协程不太好，最好还是用动画状态的退出事件来做
        isHurt = false;
    }

    public void OnDie()
    {
        // 在野猪死亡的时候，将野猪切换到 ignore raycast 层，这样已经死亡的野猪就不会和玩家产生碰撞了
        // 注意要在 Edit -> Project Settings -> Physics 2D 里面取消 ignore raycast 与 player 之间的碰撞
        gameObject.layer = 2;
        anim.SetBool("dead", true);
        isDead = true;
    }

    public void DestroyAfterAnimation()
    {
        // 其实这个方法也可以用动画状态的退出事件来调用
        Destroy(gameObject);
    }

    #endregion

    public virtual void OnDrawGizmosSelected()
    {
        // Gizmos.DrawWireSphere(transform.position + centerOffset)
        Gizmos.DrawWireCube((Vector2) transform.position + centerOffset + new Vector2(checkDistance * transform.localScale.x * -1, 0), checkSize);
    }
}
